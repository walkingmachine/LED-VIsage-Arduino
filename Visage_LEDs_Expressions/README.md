## Topics :
/control_emo </br>
   UInt8 : 1-7 </br>
	1 : content </br>
	2 : triste </br>
	3 : circonspect </br>
	4 : fache </br>
	5 : surpris </br>
	6 : coquin </br>
	7 : party </br>

/face_bright </br>
   UInt8 : 0-255 </br>

/face_mode </br>
   UInt8 : 0-4 </br>
  	0 : waiting mode </br>
	1 : wake up + loading </br>
	2 : smiling, waiting for command </br>
